(defsystem :toto
  :name "toto"
  :author "Florian Margaine <florian@margaine.com>"
  :description "Slack SLIME Client"
  :depends-on (:drakma
	       :websocket-driver
	       :jsown
	       :cl-ansi-text
	       :cl-ppcre
	       :swank
	       :local-time)
  :components ((:file "package")
	       (:file "user")
	       (:file "words")
	       (:file "transformers")
	       (:file "toto")))
