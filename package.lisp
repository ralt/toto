(defpackage #:toto
  (:use :cl)
  (:export *debug*

	   *user-token*
	   *token*
	   *self*

	   in

	   connect

	   in-channel
	   send))
