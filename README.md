# toto

```
(ql:quickload :toto)
(in-package #:toto-user)
(setf *token* "...") ;; Token stolen from web inspector in slack
(setf *user-token* "...") ;; "Legacy token" from https://api.slack.com/custom-integrations/legacy-tokens
(setf *self* "my username") ;; because reasons

(connect)
```

All the channels and nicknames are put in symbols to make
auto-completion easy in Slime. For example, on a random workspace I'm
in:

```
TOTO-USER> -general
"CD3A6TL3L"
TOTO-USER>
```

All the symbols have `-` prepended to avoid conflicts with the CL
standard.

Use `(in-channel channel &optional thread)` to change the active
channel. For example:

```
TOTO-USER> (in-channel -general)
NIL
TOTO-USER>
```

The `in` symbol is provided to remember which channel you're currently
in.

```
TOTO-USER> in
"general"
TOTO-USER>
```

For threads, given this example message from an existing thread:

```
[01:02:03] [general:work] <user1> ohai
```

We infer that the symbol `-work` is what defines the thread. We can
thus use:

```
TOTO-USER> (in-channel -general -work)
NIL
TOTO-USER> in
"general:work"
TOTO-USER>
```

Use `(send "your message")` to send a message to the active channel.

Emacs helpers:

```
(defun toto/in-channel ()
  (interactive)
  (insert "(in-channel )")
  (backward-char))

(defun toto/send ()
  (interactive)
  (insert "(send \"\")")
  (dotimes (_ 2) (backward-char)))

(add-hook
 'slime-mode-hook
 (lambda ()
   (local-set-key (kbd "C-c C-q") 'toto/in-channel)
   (local-set-key (kbd "C-c C-a") 'toto/send)))
```

TODO:

- Start new thread
- Add indication of other messages in thread buffer
- Pings?
- Load history on startup.
- Add text of slack attachments
- Add support to send a file, and/or automatically translate to snippets too big messages
- Join/leave channel
- Add an auto-loaded ~/.toto.lisp
- Add OAuth login
- Cleanup threads
