(in-package #:toto)

(defvar *transformers-in* (make-hash-table)
  "List of transformers lambdas for incoming messages.")
(defvar *transformers-out* (make-hash-table)
  "List of transformers lambdas for outgoing messages.")

(defmacro deftransformer-in (name (text) &body body)
  `(setf (gethash ',name *transformers-in*)
	 (lambda (,text)
	   ,@body)))

(defmacro deftransformer-out (name (text) &body body)
  `(setf (gethash ',name *transformers-out*)
	 (lambda (,text)
	   ,@body)))

(deftransformer-in ampersand (text)
  (ppcre:regex-replace-all "&amp;" text "&"))

(deftransformer-in greater-than (text)
  (ppcre:regex-replace-all "&gt;" text ">"))

(deftransformer-in lesser-than (text)
  (ppcre:regex-replace-all "&lt;" text "<"))

(deftransformer-in username-ping (text)
  (let ((uids (ppcre:all-matches-as-strings "<@([A-Z0-9]+)>" text)))
    (if uids
	(reduce
	 (lambda (text uid)
	   (block replace-uid
	     (handler-bind ((error #'(lambda (c)
				       (declare (ignore c))
				       ;; get-username will raise if
				       ;; the user is not found, or
				       ;; any other reason, so we can
				       ;; just always return without
				       ;; replacing.
				       (return-from replace-uid text))))
	       ;; uid will be e.g. "<@UID>", and slack API only wants
	       ;; the UID, so start by extracting that.
	       (let ((inner-uid (subseq uid 2 (- (length uid) 1))))
		 (ppcre:regex-replace-all inner-uid text (get-username inner-uid))))))
	 uids
	 :initial-value text)
	text)))

(deftransformer-in team-ping (text)
  (ppcre:regex-replace-all
   "(<!subteam\\^[A-Z0-9]+\\|)(@[\\w_]+)(>)"
   text
   "<\\2>"))

(deftransformer-in channel-ping (text)
  (ppcre:regex-replace-all
   "<#[A-Z0-9]+\\|(.+?)>"
   text
   "<#\\1>"))

(deftransformer-out username-ping (text)
  (let ((usernames (ppcre:all-matches-as-strings "-([a-zA-Z\\.0-9_-]+)" text)))
    (if usernames
	(let ((existing-users (let ((u))
				(maphash (lambda (k v) (declare (ignore k)) (push v u)) *users*)
				u)))
	  (reduce
	   (lambda (text username)
	     (let ((inner-username (subseq username 1)))
	       (if (member (subseq username 1) existing-users :test #'string=)
		   (ppcre:regex-replace-all username
					    text
					    (format nil "<@~a>"
						    (get-uid-from-username inner-username)))
		   text)))
	   usernames
	   :initial-value text))
	text)))
