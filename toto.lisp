(in-package #:toto)

(push (cons "application" "json") drakma:*text-content-types*)

(defvar *debug* nil
  "Specifies whether debug logs should be sent to a debug buffer")

(defvar *emacs-connection* nil
  "A copy of swank's emacs connection")

(defvar *user-token* nil
  "Slack user token. Unclear yet whether that is necessary.")

(defvar *token* nil
  "Slack user legacy token. Needed for API requests.")

(defvar *self* nil
  "Slack username of self. Probably fetchable through the API.")

(defvar *client* nil
  "The websocket client.")

(defvar *channels-lock* (bt:make-lock)) ;; Guards the following:
(defvar *channels* (make-hash-table :test #'equal)
  "List of channels that sent a message.")

(defvar *users-lock* (bt:make-lock)) ;; Guards the following:
(defvar *users* (make-hash-table :test #'equal)
  "List of users that sent a message.")

(defvar *bots-lock* (bt:make-lock)) ;; Guards the following:
(defvar *bots* (make-hash-table :test #'equal)
  "List of bots that sent a message.")

(defvar *threads-lock* (bt:make-lock)) ;; Guards the following:
(defvar *threads* (make-hash-table :test #'equal)
  "List of Slack threads that sent a message.")
(defvar *threads-queue* (make-hash-table :test #'equal)
  "Queue where thread messages are temporarily stored until the first
  message arrives.")

(defvar *messages-queue-lock* (bt:make-lock)) ;; Guards the following:
(defvar *messages-queue* nil
  "Queue where messages are sent to be displayed in emacs.")
(defvar *messages-queue-variable* (bt:make-condition-variable))

(defvar in ""
  "Human-readable current channel where messages are sent.")

(defvar *current-channel* nil
  "Current channel where messages are sent.")
(defvar *current-thread* nil
  "Current thread where messages are sent, if any.")

(defvar *channels-list-lock* (bt:make-lock)) ;; Guards the following:
(defvar *last-channel-list* nil
  "Time when the last channel list was fetched.")
(defvar *channels-list* nil
  "Full list of Slack channels.")

(defvar *ws-replies-lock* (bt:make-lock)) ;; Guards the following:
(defvar *ws-replies-counter* 0
  "Counter used as an identifier to websocket messages sent to Slack RTM API.")
(defvar *ws-replies* (make-hash-table :test #'equal)
  "In-flight identifiers of messages sent to Slack RTM API.")

(defvar *channels-list-refresh-interval* 36000
  "Interval after which we should refresh the full list of channels.")

(defvar *message-insertion-thread* nil
  "Thread where messages are sent to emacs.")

(defclass event ()
  ((type :type string :reader event-type :initform "message")
   (ts :type string :reader event-ts :initarg :ts)
   (channel :type string :accessor event-channel :initarg :channel)
   (text :type string :reader event-text :initarg :text)
   (user :type string :reader event-user)
   (username :type string :reader event-username :initform nil :initarg :username)
   (hidden :type boolean :reader event-hidden :initform nil)
   (thread-ts :type string :reader event-thread-ts :initform nil :initarg :thread-ts)
   (bot-id :type string :reader event-bot-id :initform nil)
   (message :reader event-message :initform nil)))

(defclass message-part ()
  ((text :type string :reader text :initarg :text)
   (color :type string :reader color :initarg :color)))

(defclass message ()
  ((parts :type list :reader parts :initarg :parts)
   (channel :type string :reader channel :initarg :channel)))

(defvar *handlers* (make-hash-table :test #'equal))

(defmacro defhandler (name vars &body body)
  `(progn
     (defun ,name ,vars
       ,@body)
     (setf (gethash (string-downcase (symbol-name ',name)) *handlers*)
	   #',name)))

(defun publish-event (event &key hidden)
  (bt:with-recursive-lock-held (*threads-lock*)
    (let* ((thread-ts (event-thread-ts event))
	   (new-thread (and thread-ts
			    (not (member thread-ts
					 (loop for key being the hash-keys of *threads-queue*
					    collect key)
					 :test #'equal)))))
      (when new-thread
	(return-from publish-event
	  (setf (gethash thread-ts *threads-queue*)
		(append (gethash thread-ts *threads-queue*)
			(list event)))))))

  (let* ((username (or (event-username event)
		       (when (event-bot-id event) (get-bot-name (event-bot-id event)))
		       (get-username (event-user event))))
	 (channel (get-channel (event-channel event)))
	 (thread-ts (event-thread-ts event))
	 (in-thread (not (eq nil thread-ts)))
	 (timestamp (local-time:unix-to-timestamp
		     (parse-integer (event-ts event) :junk-allowed t)))
	 (formatted-timestamp (local-time:format-timestring
			       nil
			       timestamp
			       :format '((:hour 2) ":" (:min 2) ":" (:sec 2))))
	 (pipe-timestamp (local-time:format-timestring
			  nil
			  timestamp
			  :format '((:year 2) "/" (:month 2) "/" (:day 2) " "
				    (:hour 2) ":" (:min 2) ":" (:sec 2))))
	 (displayed-channel (format nil "~a~a"
				    channel
				    (if in-thread
					(format nil ":~a" (get-thread (event-thread-ts event)))
					"")))
	 (formatted-message (slack-format (event-text event)))

	 (pipe-message
	  (make-instance 'message
			 :channel displayed-channel
			 :parts (list (make-instance
				       'message-part
				       :color "orange3"
				       :text (format nil "[~a] " pipe-timestamp))
				      (make-instance
				       'message-part
				       :color "dim grey"
				       :text (format nil "<~a> " username))
				      (make-instance
				       'message-part
				       :color "green"
				       :text (format nil "~a~%" formatted-message)))))
	 (message
	  (format
	   nil "[~a] [~a] <~a> ~a~%"
	   (cl-ansi-text:yellow formatted-timestamp)
	   (cl-ansi-text:white displayed-channel)
	   username
	   (cl-ansi-text:green formatted-message))))

    (unless hidden
      (format t "~a" message))
    (bt:with-recursive-lock-held (*messages-queue-lock*)
      (setf *messages-queue* (append *messages-queue*
				     (list pipe-message)))
      (bt:condition-notify *messages-queue-variable*))))

(defhandler message (event)
  (when (and (event-hidden event)
	     (event-message event)
	     (jsown:keyp (event-message event) "thread_ts"))
    (let* ((message (event-message event))
	   (thread-ts (jsown:val message "thread_ts")))
      (bt:with-recursive-lock-held (*threads-lock*)
       (when (gethash thread-ts *threads-queue*)
	 (let ((new-event (event-from-message message)))
	   (setf (event-channel new-event) (event-channel event))
	   (publish-event new-event :hidden t))
	 (dolist (event (gethash thread-ts *threads-queue*))
	   (publish-event event))
	 (setf (gethash thread-ts *threads-queue*) nil)))))
  (unless (event-hidden event)
    (publish-event event)))

(defun slack-format (text)
  (maphash (lambda (name fn)
	     (declare (ignore name))
	     (setf text (funcall fn text)))
	   *transformers-in*)
  text)

(defun %get (where what how)
  (multiple-value-bind (value presentp)
      (gethash what where)
    (if presentp
	value
	(setf (gethash what where) (funcall how what)))))

(defun get-bot-name (bot-id)
  (bt:with-recursive-lock-held (*bots-lock*)
    (%get *bots* bot-id #'fetch-bot-name)))

(defun fetch-bot-name (bot-id)
  (when (string= bot-id "B01")
    ;; slackbot is a special case
    (return-from fetch-bot-name "slackbot"))
  (let ((bot-info (jsown:parse
		   (drakma:http-request
		    (format nil "https://slack.com/api/bots.info?token=~a&bot=~a"
			    *user-token* bot-id)))))
    (jsown:val (jsown:val bot-info "bot") "name")))

(defun get-thread (thread-ts)
  (bt:with-recursive-lock-held (*threads-lock*)
    (let* ((thread-nickname
	    (%get *threads*
		  thread-ts
		  (lambda (ts)
		    (declare (ignore ts))
		    (word))))
	   (thread-symbol (intern (cat "-" (string-upcase thread-nickname)) "TOTO-USER")))
      (proclaim `(special ,thread-symbol))
      (setf (symbol-value thread-symbol) thread-ts)
      thread-nickname)))

(defun in-channel (channel &optional thread)
  (setf in (format nil "~a~a"
		   (gethash channel *channels-list*)
		   (if thread
		       (format nil ":~a" (gethash thread *threads*))
		       "")))
  (setf *current-channel* channel)
  (setf *current-thread* thread))

(defun list-channels ()
  (if (channels-list-is-fresh)
      *channels-list*
      (refresh-channels-list)))

(defun channels-list-is-fresh ()
  (and *last-channel-list*
       (> *last-channel-list* (- (get-universal-time) *channels-list-refresh-interval*))))

(defun refresh-channels-list ()
  (format t "Refreshing channels list...~%")
  (bt:with-recursive-lock-held (*channels-list-lock*)
    (unwind-protect
	 (let ((channels (make-hash-table :test #'equal))
	       (cursor ""))
	   (loop
	      (let ((channels-info
		     (jsown:parse
		      (drakma:http-request
		       (format nil
			       "https://slack.com/api/conversations.list?token=~a&types=public_channel,private_channel,mpim,im&cursor=~a&limit=100"
			       *user-token*
			       cursor)))))
		(loop for chan in (jsown:val channels-info "channels")
		   do (setf (gethash (jsown:val chan "id") channels)
			    (if (jsown:val chan "is_im")
				(get-username (jsown:val chan "user"))
				(jsown:val chan "name"))))

		(let ((response-metadata (jsown:val channels-info "response_metadata")))
		  (when (not (jsown:keyp response-metadata "next_cursor"))
		    (return-from refresh-channels-list (intern-channels (setf *channels-list* channels))))
		  (let ((next-cursor (jsown:val response-metadata "next_cursor")))
		    (unless next-cursor
		      (return-from refresh-channels-list (intern-channels (setf *channels-list* channels))))
		    (when (string= next-cursor "")
		      (return-from refresh-channels-list (intern-channels (setf *channels-list* channels))))))

		(setf cursor (jsown:val (jsown:val channels-info "response_metadata") "next_cursor")))))
      (setf *last-channel-list* (get-universal-time)))))

(defun cat (&rest args)
  (apply #'concatenate 'string args))

(defun intern-channel (name id)
  (proclaim `(special ,(intern (string-upcase (cat "-" name)) "TOTO-USER")))
  (setf (symbol-value (intern (string-upcase (cat "-" name)) "TOTO-USER")) id))

(defun intern-channels (channels)
  (loop for name being the hash-values in channels using (hash-key id)
     do (intern-channel name id))
  channels)

(defun send (message)
  "Send a message. Channel is *current-channel*."
  (maphash (lambda (name fn)
	     (declare (ignore name))
	     (setf message (funcall fn message)))
	   *transformers-out*)
  (bt:with-recursive-lock-held (*ws-replies-lock*)
    (let ((counter (incf *ws-replies-counter*)))
      (wsd:send *client*
		(jsown:to-json
		 `(:obj
		   ("id" . ,counter)
		   ("type" . "message")
		   ("channel" . ,*current-channel*)
		   ("text" . ,message)
		   ,@(when *current-thread*
		       `(("thread_ts" . ,*current-thread*))))))
      (setf (gethash counter *ws-replies*) (list *current-channel* *current-thread*))
      t)))

(defun get-uid-from-username (username)
  "TODO: Find a way to get rid of that. It's ugly."
  (bt:with-recursive-lock-held (*users-lock*)
    (maphash (lambda (uid user)
	       (when (string= user username)
		 (return-from get-uid-from-username uid)))
	     *users*)))

(defun get-username (uid)
  (bt:with-recursive-lock-held (*users-lock*)
    (%get *users* uid #'fetch-user)))

(defun get-channel (id)
  (bt:with-recursive-lock-held (*channels-lock*)
    (multiple-value-bind (value presentp)
	(gethash id *channels*)
      (if presentp
	  value
	  (let ((channel (setf (gethash id *channels*) (fetch-channel id))))
	    (intern-channel channel id)
	    channel)))))

(defun fetch-user (uid)
  (let ((user-info (jsown:parse (drakma:http-request
				 "https://slack.com/api/users.info"
				 :method :post
				 :content-type "application/x-www-form-urlencoded"
				 :content (format nil "token=~a&user=~a"
						  *user-token* uid)))))
    (jsown:val (jsown:val user-info "user") "name")))

(defun fetch-channel (id)
  (let ((channel-info (jsown:val
		       (jsown:parse
			(drakma:http-request
			 (format nil "https://slack.com/api/conversations.info?token=~a&channel=~a"
				 *user-token* id)))
		       "channel")))
    (if (jsown:val channel-info "is_im")
	(get-username (jsown:val channel-info "user"))
	(jsown:val channel-info "name_normalized"))))

(defun message-insertion-loop ()
  (loop
     (bt:with-recursive-lock-held (*messages-queue-lock*)
       (block loop
	 (loop
	    (when (= (length *messages-queue*) 0)
	      (return-from loop))
	    (let ((message (pop *messages-queue*)))
	      (swank::with-connection (*emacs-connection*)
		(swank:eval-in-emacs
		 `(let ((buffer (get-buffer-create ,(format nil "*toto<~a>*" (channel message)))))
		    (with-current-buffer buffer
		      (unless (bound-and-true-p font-lock-mode)
			(turn-on-font-lock))
		      (end-of-buffer)
		      ,@(loop for part in (parts message)
			   collect `(insert (propertize ,(text part)
							'font-lock-face
							(quote (:foreground ,(color part))))))))
		 t)))))
       (bt:condition-wait *messages-queue-variable* *messages-queue-lock*))))

(defun connect ()
  (setf *ws-replies* (make-hash-table :test #'equal))
  (when *debug*
    (uiop:run-program "rm -f /tmp/toto-pipe && mkfifo /tmp/toto-pipe")
    (swank:eval-in-emacs
     '(let ((buffer (generate-new-buffer "*toto-debug*")))
       (with-current-buffer buffer
	 (start-process "toto" buffer "bash" "-c" "stdbuf -o0 tail -f /tmp/toto-pipe")))
     t))
  (when *message-insertion-thread*
    (bt:destroy-thread *message-insertion-thread*)
    (setf *message-insertion-thread* nil))
  (setf *message-insertion-thread*
	(bt:make-thread #'message-insertion-loop
			:initial-bindings `((*emacs-connection* . ,swank::*emacs-connection*))
			:name "Message insertion"))
  (let* ((data (jsown:parse
		(drakma:http-request
		 (format nil "https://slack.com/api/rtm.connect?token=~a" *token*))))
	 (url (jsown:val data "url")))
    (when *client*
      (wsd:close-connection *client* "Re-connecting" 1)
      (setf *client* nil))
    (setf *client* (wsd:make-client url))
    (wsd:start-connection *client*)
    (wsd:on :error *client* (lambda (error)
			      (format t "Got an error: ~a~%" error)))
    (wsd:on :message *client* (lambda (message)
				(on-message message)))
    (wsd:on :close *client* (lambda (&key code reason)
			      (format t "Closed because '~a' (code: ~a)~%" reason code)))
    ;; Listing channels is fairly slow, so do that after everything is
    ;; setup, so that at least we can see the incoming messages.
    (list-channels)))

(defun d (str &rest args)
  (when *debug*
    (with-open-file (s "/tmp/toto-pipe"
		       :direction :output
		       :if-exists :append)
      (format s "~a~%~%" (apply #'format nil str args)))))

(defun event-from-message (message)
  (let ((event (make-instance 'event)))
    (loop for key in (jsown:keywords message)
       do (let ((slot-name (intern (string-upcase (substitute #\- #\_ key)) "TOTO")))
	    (when (slot-exists-p event slot-name)
	      (setf (slot-value
		     event
		     slot-name)
		    (jsown:val message key)))))
    event))

(defun on-message (message)
  (d "~a" message)
  (let ((parsed (jsown:parse message)))
    (when (jsown:keyp parsed "reply_to")
      (let ((channel)
	    (thread))
	(bt:with-recursive-lock-held (*ws-replies-lock*)
	  (let ((message-id (jsown:val parsed "reply_to")))
	    (destructuring-bind (saved-channel saved-thread)
		(gethash message-id *ws-replies*)
	      (setf channel saved-channel)
	      (setf thread saved-thread))
	    (remhash message-id *ws-replies*)))
	(handler-case
	    (funcall
	     (gethash "message" *handlers*)
	     (let ((event (make-instance 'event
					 :ts (jsown:val parsed "ts")
					 :channel channel
					 :text (jsown:val parsed "text")
					 :thread-ts thread
					 :username *self*)))
	       event))
	(error (e)
	  (d "Callback error: ~a" e)))))
    (when (jsown:keyp parsed "type")
      (let ((callback (gethash (jsown:val parsed "type") *handlers*)))
	(when callback
	  (handler-case
	      (funcall
	       callback
	       (event-from-message parsed))
	    (error (e)
	      (d "Callback error: ~a" e))))))))
